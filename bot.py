from dotenv import load_dotenv

load_dotenv(
    "config.env",
    override=True,
)
import asyncio
import os
import shutil
import time

import psutil
import pyromod
from PIL import Image
from pyrogram import Client, filters,enums
from pyrogram.errors import (
    FloodWait,
    InputUserDeactivated,
    PeerIdInvalid,
    UserIsBlocked,
)
from pyrogram.types import (
    CallbackQuery,
    InlineKeyboardButton,
    InlineKeyboardMarkup,
    Message,
    User,
)

from __init__ import (
    AUDIO_EXTENSIONS,
    BROADCAST_MSG,
    LOGGER,
    MERGE_MODE,
    SUBTITLE_EXTENSIONS,
    UPLOAD_AS_DOC,
    UPLOAD_TO_DRIVE,
    VIDEO_EXTENSIONS,
    bMaker,
    formatDB,
    gDict,
    queueDB,
    replyDB,
)
from config import Config
from helpers import database
from helpers.utils import UserSettings, get_readable_file_size, get_readable_time

botStartTime = time.time()
parent_id = Config.GDRIVE_FOLDER_ID


class MergeBot(Client):
    def start(self):
        super().start()
        try:
            self.send_message(chat_id=int(Config.OWNER), text="<b>Bᴏᴛ Sᴛᴀʀᴛᴇᴅ!</b>")
        except Exception as err:
            LOGGER.error("Boot alert failed! Please start bot in PM")
        return LOGGER.info("Bᴏᴛ Sᴛᴀʀᴛᴇᴅ!")

    def stop(self):
        super().stop()
        return LOGGER.info("Bᴏᴛ Sᴛᴏᴘᴘᴇᴅ")


mergeApp = MergeBot(
    name="merge-bot",
    api_hash=Config.API_HASH,
    api_id=Config.TELEGRAM_API,
    bot_token=Config.BOT_TOKEN,
    workers=300,
    plugins=dict(root="plugins"),
    app_version="5.0+yash-mergebot",
)


if os.path.exists("downloads") == False:
    os.makedirs("downloads")


@mergeApp.on_message(filters.command(["log"]) & filters.user(Config.OWNER_USERNAME))
async def sendLogFile(c: Client, m: Message):
    await m.reply_document(document="./mergebotlog.txt")
    return


@mergeApp.on_message(filters.command(["login"]) & filters.private)
async def loginHandler(c: Client, m: Message):
    user = UserSettings(m.from_user.id, m.from_user.first_name)
    if user.banned:
        await m.reply_text(text=f"**Bᴀɴɴᴇᴅ Uꜱᴇʀ Dᴇᴛᴇᴄᴛᴇᴅ!**\n**Sᴏʀʀʏ Yᴏᴜ Cᴀɴ'ᴛ Uꜱᴇ Mᴇ!!**\n\n**Cᴏɴᴛᴀᴄᴛ:** {Config.OWNER_USERNAME}", quote=True)
        return
    if user.user_id == int(Config.OWNER):
        user.allowed = True
    if user.allowed:
        await m.reply_text(text=f"**Dᴏɴ'ᴛ Sᴘᴀᴍ**\n**Yᴏᴜ Cᴀɴ Uꜱᴇ Mᴇ!!**⚡", quote=True)
    else:
        try:
            passwd = m.text.split(" ", 1)[1]
        except:
            await m.reply_text("**Command:**\n  `/login <password>`\n\n**Usage:**\n  `password`: Get the password from owner",quote=True,parse_mode=enums.parse_mode.ParseMode.MARKDOWN)
        passwd = passwd.strip()
        if passwd == Config.PASSWORD:
            user.allowed = True
            await m.reply_text(
                text=f"**Lᴏɢɪɴ Pᴀꜱꜱᴇᴅ ️‍🔥,**\n**Nᴏᴡ Yᴏᴜ Cᴀɴ Uꜱᴇ Mᴇ!!** ⚡ ", quote=True
            )
        else:
            await m.reply_text(
                text=f"**Lᴏɢɪɴ Fᴀɪʟᴇᴅ 😔,**\n**Yᴏᴜ Cᴀɴ'ᴛ Uꜱᴇ Mᴇ!!**\n\n**Contact:** {Config.OWNER_USERNAME}",
                quote=True,
            )
    user.set()
    del user
    return


@mergeApp.on_message(filters.command(["stats"]) & filters.private)
async def stats_handler(c: Client, m: Message):
    currentTime = get_readable_time(time.time() - botStartTime)
    total, used, free = shutil.disk_usage(".")
    total = get_readable_file_size(total)
    used = get_readable_file_size(used)
    free = get_readable_file_size(free)
    sent = get_readable_file_size(psutil.net_io_counters().bytes_sent)
    recv = get_readable_file_size(psutil.net_io_counters().bytes_recv)
    cpuUsage = psutil.cpu_percent(interval=0.5)
    memory = psutil.virtual_memory().percent
    disk = psutil.disk_usage("/").percent
    stats = (
        f"<b>**╭Bᴏᴛ Sᴛᴀᴛɪꜱᴛɪᴄꜱ**</b>\n"
        f"<b>**│</b>\n"
        f"<b>**├Bᴏᴛ Uᴘᴛɪᴍᴇ : {currentTime}**</b>\n"
        f"<b>**├Tᴏᴛᴀʟ Dɪꜱᴋ Sᴘᴀᴄᴇ : {total}**</b>\n"
        f"<b>**├Tᴏᴛᴀʟ Uꜱᴇᴅ Sᴘᴀᴄᴇ : {used}**</b>\n"
        f"<b>**├Tᴏᴛᴀʟ Fʀᴇᴇ Sᴘᴀᴄᴇ : {free}**</b>\n"
        f"<b>**├Tᴏᴛᴀʟ Uᴘʟᴏᴀᴅ : {sent}**</b>\n"
        f"<b>**├Tᴏᴛᴀʟ Dᴏᴡɴʟᴏᴀᴅ : {recv}**</b>\n"
        f"<b>**├Cᴘᴜ : {cpuUsage}%**</b>\n"
        f"<b>**├Rᴀᴍ : {memory}%</b>**\n"
        f"<b>**╰Dɪꜱᴋ : {disk}%**</b>\n"
    )
    await m.reply_text(text=stats, quote=True)


@mergeApp.on_message(
    filters.command(["broadcast"])
    & filters.private
    & filters.user(Config.OWNER_USERNAME)
)
async def broadcast_handler(c: Client, m: Message):
    msg = m.reply_to_message
    userList = await database.broadcast()
    len = userList.collection.count_documents({})
    status = await m.reply_text(text=BROADCAST_MSG.format(str(len), "0"), quote=True)
    success = 0
    for i in range(len):
        try:
            uid = userList[i]["_id"]
            if uid != int(Config.OWNER):
                await msg.copy(chat_id=uid)
            success = i + 1
            await status.edit_text(text=BROADCAST_MSG.format(len, success))
            LOGGER.info(f"Message sent to {userList[i]['name']} ")
        except FloodWait as e:
            await asyncio.sleep(e.x)
            await msg.copy(chat_id=userList[i]["_id"])
            LOGGER.info(f"Message sent to {userList[i]['name']} ")
        except InputUserDeactivated:
            await database.deleteUser(userList[i]["_id"])
            LOGGER.info(f"{userList[i]['_id']} - {userList[i]['name']} : deactivated\n")
        except UserIsBlocked:
            await database.deleteUser(userList[i]["_id"])
            LOGGER.info(
                f"{userList[i]['_id']} - {userList[i]['name']} : blocked the bot\n"
            )
        except PeerIdInvalid:
            await database.deleteUser(userList[i]["_id"])
            LOGGER.info(
                f"{userList[i]['_id']} - {userList[i]['name']} : user id invalid\n"
            )
        except Exception as err:
            LOGGER.warning(f"{err}\n")
        await asyncio.sleep(3)
    await status.edit_text(
        text=BROADCAST_MSG.format(len, success)
        + f"**Fᴀɪʟᴇᴅ: {str(len-success)}**\n\n__🤓 **Bʀᴏᴀᴅᴄᴀꜱᴛ Cᴏᴍᴘʟᴇᴛᴇᴅ Sᴜᴄᴇꜱꜱꜰᴜʟʟʏ__**",
    )


@mergeApp.on_message(filters.command(["start"]) & filters.private)
async def start_handler(c: Client, m: Message):
    user = UserSettings(m.from_user.id, m.from_user.first_name)

    if m.from_user.id != int(Config.OWNER):
        if user.allowed is False:
            res = await m.reply_text(
                text=f"**Hɪ {m.from_user.first_name}**\n\n**Sᴏʀʀʏ Yᴏᴜ Cᴀɴ'ᴛ Uꜱᴇ Mᴇ**\n\n**Cᴏɴᴛᴀᴄᴛ: {Config.OWNER_USERNAME}** ",
                quote=True,
            )
            return
    else:
        user.allowed = True
        user.set()
    res = await m.reply_text(
        text=f"**Hɪ {m.from_user.first_name}**\n\n**I Aᴍ A Fɪʟᴇ/Vɪᴅᴇᴏ Mᴇʀɢᴇʀ Bᴏᴛ ⚡**\n\n**I Cᴀɴ Mᴇʀɢᴇ Tᴇʟᴇɢʀᴀᴍ Fɪʟᴇꜱ!, Aɴᴅ Uᴘʟᴏᴀᴅ Iᴛ Tᴏ Tᴇʟᴇɢʀᴀᴍ 🔥**\n\n**Oᴡɴᴇʀ: {Config.OWNER_USERNAME}** ",
        quote=True,
    )
    del user


@mergeApp.on_message(
    (filters.document | filters.video | filters.audio) & filters.private
)
async def files_handler(c: Client, m: Message):
    user_id = m.from_user.id
    user = UserSettings(user_id, m.from_user.first_name)
    if user_id != int(Config.OWNER):
        if user.allowed is False:
            res = await m.reply_text(
                text=f"**Hɪ {m.from_user.first_name}**\n\n**Sᴏʀʀʏ Yᴏᴜ Cᴀɴ'ᴛ Uꜱᴇ Mᴇ**\n\n**Cᴏɴᴛᴀᴄᴛ: {Config.OWNER_USERNAME}** ",
                quote=True,
            )
            return
    if user.merge_mode == 4: # extract_mode
        return
    input_ = f"downloads/{str(user_id)}/input.txt"
    if os.path.exists(input_):
        await m.reply_text("**Sᴏʀʀʏ Bʀᴏ,**\n**Aʟʀᴇᴀᴅʏ Oɴᴇ Pʀᴏᴄᴇꜱꜱ Iɴ Pʀᴏɢʀᴇꜱꜱ!**\n**Dᴏɴ'ᴛ Sᴘᴀᴍ.**")
        return
    media = m.video or m.document or m.audio
    if media.file_name is None:
        await m.reply_text("Fɪʟᴇ Nᴏᴛ Fᴏᴜɴᴅ")
        return
    currentFileNameExt = media.file_name.rsplit(sep=".")[-1].lower()
    if currentFileNameExt in "conf":
        await m.reply_text(
            text="**💾 Cᴏɴꜰɪɢ Fɪʟᴇ Fᴏᴜɴᴅ, Dᴏ Yᴏᴜ Wᴀɴᴛ Tᴏ Sᴀᴠᴇ Iᴛ?**",
            reply_markup=InlineKeyboardMarkup(
                [
                    [
                        InlineKeyboardButton("✅ Yes", callback_data=f"rclone_save"),
                        InlineKeyboardButton("❌ No", callback_data="rclone_discard"),
                    ]
                ]
            ),
            quote=True,
        )
        return
    # if MERGE_MODE.get(user_id) is None:
    #     userMergeMode = database.getUserMergeSettings(user_id)
    #     if userMergeMode is not None:
    #         MERGE_MODE[user_id] = userMergeMode
    #     else:
    #         database.setUserMergeMode(uid=user_id, mode=1)
    #         MERGE_MODE[user_id] = 1

    if user.merge_mode == 1:

        if queueDB.get(user_id, None) is None:
            formatDB.update({user_id: currentFileNameExt})
        if formatDB.get(
            user_id, None
        ) is not None and currentFileNameExt != formatDB.get(user_id):
            await m.reply_text(
                f"**Fɪʀꜱᴛ Yᴏᴜ Sᴇɴᴛ A {formatDB.get(user_id).upper()} Fɪʟᴇ Sᴏ Nᴏᴡ Sᴇɴᴅ Oɴʟʏ Tʜᴀᴛ Tʏᴘᴇ Oꜰ Fɪʟᴇ.**",
                quote=True,
            )
            return
        if currentFileNameExt not in VIDEO_EXTENSIONS:
            await m.reply_text(
                "**Tʜɪꜱ Vɪᴅᴇᴏ Fᴏʀᴍᴀᴛ Nᴏᴛ Aʟʟᴏᴡᴇᴅ!**\n**Oɴʟʏ Sᴇɴᴅ MP4 or MKV or WEBM.**",
                quote=True,
            )
            return
        editable = await m.reply_text("Please Wait ...", quote=True)
        MessageText = "**Oᴋᴀʏ,**\nSᴇɴᴅ Mᴏʀᴇ Vɪᴅᴇᴏs Tᴏ Mᴇʀɢᴇ... Oʀ Pʀᴇss **Mᴇʀɢᴇ Nᴏᴡ** Bᴜᴛᴛᴏɴ"

        if queueDB.get(user_id, None) is None:
            queueDB.update({user_id: {"videos": [], "subtitles": [], "audios": []}})
        if (
            len(queueDB.get(user_id)["videos"]) >= 0
            and len(queueDB.get(user_id)["videos"]) < 10
        ):
            queueDB.get(user_id)["videos"].append(m.id)
            queueDB.get(m.from_user.id)["subtitles"].append(None)

            # LOGGER.info(
            #     queueDB.get(user_id)["videos"], queueDB.get(m.from_user.id)["subtitles"]
            # )

            if len(queueDB.get(user_id)["videos"]) == 1:
                reply_ = await editable.edit(
                    "**Sᴇɴᴅ Sᴏᴍᴇ Mᴏʀᴇ Vɪᴅᴇᴏs Tᴏ Mᴇʀɢᴇ Tᴏɢᴇᴛʜᴇʀ...**",
                    reply_markup=InlineKeyboardMarkup(
                        bMaker.makebuttons(["Cancel"], ["cancel"])
                    ),
                )
                replyDB.update({user_id: reply_.id})
                return
            if queueDB.get(user_id, None)["videos"] is None:
                formatDB.update({user_id: currentFileNameExt})
            if replyDB.get(user_id, None) is not None:
                await c.delete_messages(
                    chat_id=m.chat.id, message_ids=replyDB.get(user_id)
                )
            if len(queueDB.get(user_id)["videos"]) == 10:
                MessageText = "Oᴋᴀʏ, Nᴏᴡ Jᴜꜱᴛ Pʀᴇꜱꜱ **Mᴇʀɢᴇ Nᴏᴡ** Bᴜᴛᴛᴏɴ Pʟᴏx!"
            markup = await makeButtons(c, m, queueDB)
            reply_ = await editable.edit(
                text=MessageText, reply_markup=InlineKeyboardMarkup(markup)
            )
            replyDB.update({user_id: reply_.id})
        elif len(queueDB.get(user_id)["videos"]) > 10:
            markup = await makeButtons(c, m, queueDB)
            await editable.text(
                "**Mᴀx 10 Vɪᴅᴇᴏꜱ Aʟʟᴏᴡᴇᴅ**", reply_markup=InlineKeyboardMarkup(markup)
            )

    elif user.merge_mode == 2:
        editable = await m.reply_text("Please Wait ...", quote=True)
        MessageText = (
            "**Oᴋᴀʏ,**\nNᴏᴡ Sᴇɴᴅ Mᴇ Sᴏᴍᴇ Mᴏʀᴇ <u>Aᴜᴅɪᴏꜱ</u> Oʀ Pʀᴇꜱꜱ **Mᴇʀɢᴇ Nᴏᴡ** Bᴜᴛᴛᴏɴ!"
        )

        if queueDB.get(user_id, None) is None:
            queueDB.update({user_id: {"videos": [], "subtitles": [], "audios": []}})
        if len(queueDB.get(user_id)["videos"]) == 0:
            queueDB.get(user_id)["videos"].append(m.id)
            # if len(queueDB.get(user_id)["videos"])==1:
            reply_ = await editable.edit(
                text="**Nᴏᴡ, Sᴇɴᴅ Aʟʟ Tʜᴇ Aᴜᴅɪᴏꜱ Yᴏᴜ Wᴀɴᴛ Tᴏ Mᴇʀɢᴇ**",
                reply_markup=InlineKeyboardMarkup(
                    bMaker.makebuttons(["Cancel"], ["cancel"])
                ),
            )
            replyDB.update({user_id: reply_.id})
            return
        elif (
            len(queueDB.get(user_id)["videos"]) >= 1
            and currentFileNameExt in AUDIO_EXTENSIONS
        ):
            queueDB.get(user_id)["audios"].append(m.id)
            if replyDB.get(user_id, None) is not None:
                await c.delete_messages(
                    chat_id=m.chat.id, message_ids=replyDB.get(user_id)
                )
            markup = await makeButtons(c, m, queueDB)

            reply_ = await editable.edit(
                text=MessageText, reply_markup=InlineKeyboardMarkup(markup)
            )
            replyDB.update({user_id: reply_.id})
        else:
            await m.reply("**Tʜɪꜱ Fɪʟᴇᴛʏᴘᴇ Iꜱ Nᴏᴛ Vᴀʟɪᴅ**")
            return

    elif user.merge_mode == 3:

        editable = await m.reply_text("Pʟᴇᴀꜱᴇ Wᴀɪᴛ ...", quote=True)
        MessageText = "**Oᴋᴀʏ,**\nNᴏᴡ Sᴇɴᴅ Mᴇ Sᴏᴍᴇ Mᴏʀᴇ <u>Sᴜʙᴛɪᴛʟᴇꜱ</u> Oʀ Pʀᴇꜱꜱ **Mᴇʀɢᴇ Nᴏᴡ** Bᴜᴛᴛᴏɴ!"
        if queueDB.get(user_id, None) is None:
            queueDB.update({user_id: {"videos": [], "subtitles": [], "audios": []}})
        if len(queueDB.get(user_id)["videos"]) == 0:
            queueDB.get(user_id)["videos"].append(m.id)
            # if len(queueDB.get(user_id)["videos"])==1:
            reply_ = await editable.edit(
                text="**Nᴏᴡ, Sᴇɴᴅ Aʟʟ Tʜᴇ Sᴜʙᴛɪᴛʟᴇꜱ Yᴏᴜ Wᴀɴᴛ Tᴏ Mᴇʀɢᴇ**",
                reply_markup=InlineKeyboardMarkup(
                    bMaker.makebuttons(["Cancel"], ["cancel"])
                ),
            )
            replyDB.update({user_id: reply_.id})
            return
        elif (
            len(queueDB.get(user_id)["videos"]) >= 1
            and currentFileNameExt in SUBTITLE_EXTENSIONS
        ):
            queueDB.get(user_id)["subtitles"].append(m.id)
            if replyDB.get(user_id, None) is not None:
                await c.delete_messages(
                    chat_id=m.chat.id, message_ids=replyDB.get(user_id)
                )
            markup = await makeButtons(c, m, queueDB)

            reply_ = await editable.edit(
                text=MessageText, reply_markup=InlineKeyboardMarkup(markup)
            )
            replyDB.update({user_id: reply_.id})
        else:
            await m.reply("**Tʜɪꜱ Fɪʟᴇᴛʏᴘᴇ Iꜱ Nᴏᴛ Vᴀʟɪᴅ**")
            return


@mergeApp.on_message(filters.photo & filters.private)
async def photo_handler(c: Client, m: Message):
    user = UserSettings(m.chat.id, m.from_user.first_name)
    # if m.from_user.id != int(Config.OWNER):
    if not user.allowed:
        res = await m.reply_text(
            text=f"**Hɪ {m.from_user.first_name}**\n\n**Sᴏʀʀʏ Yᴏᴜ Cᴀɴ'ᴛ Uꜱᴇ Mᴇ**\n\n**Cᴏɴᴛᴀᴄᴛ: {Config.OWNER_USERNAME}** ",
            quote=True,
        )
        del user
        return
    thumbnail = m.photo.file_id
    msg = await m.reply_text("**Sᴀᴠɪɴɢ Tʜᴜᴍʙɴᴀɪʟ. . . .**", quote=True)
    user.thumbnail = thumbnail
    user.set()
    # await database.saveThumb(m.from_user.id, thumbnail)
    LOCATION = f"downloads/{m.from_user.id}_thumb.jpg"
    await c.download_media(message=m, file_name=LOCATION)
    await msg.edit_text(text="**Cᴜꜱᴛᴏᴍ Tʜᴜᴍʙɴᴀɪʟ Sᴀᴠᴇᴅ! 🔥**")
    del user


@mergeApp.on_message(filters.command(["extract"]) & filters.private)
async def media_extracter(c: Client, m: Message):
    user = UserSettings(uid=m.from_user.id, name=m.from_user.first_name)
    if not user.allowed:
        return
    if user.merge_mode == 4:
        if m.reply_to_message is None:
            await m.reply(text="**Rᴇᴘʟʏ** /extract **Tᴏ A Vɪᴅᴇᴏ Oʀ Dᴏᴄᴜᴍᴇɴᴛ Fɪʟᴇ**")
            return
        rmess = m.reply_to_message
        if rmess.video or rmess.document:
            media = rmess.video or rmess.document
            mid=rmess.id
            file_name = media.file_name
            if file_name is None:
                await m.reply("**Fɪʟᴇ Nᴀᴍᴇ Nᴏᴛ Fᴏᴜɴᴅ; Gᴏᴛᴏ** @anshXbot0")
                return
            markup = bMaker.makebuttons(
                set1=["Audio", "Subtitle", "Cancel"],
                set2=[f"extract_audio_{mid}", f"extract_subtitle_{mid}", 'cancel'],
                isCallback=True,
                rows=2,
            )
            await m.reply(
                text="**Cʜᴏᴏꜱᴇ Fʀᴏᴍ Bᴇʟᴏᴡ Wʜᴀᴛ Yᴏᴜ Wᴀɴᴛ Tᴏ Exᴛʀᴀᴄᴛ?**",
                quote=True,
                reply_markup=InlineKeyboardMarkup(markup),
            )
    else:
        await m.reply(
            text="**Cʜᴀɴɢᴇ Sᴇᴛᴛɪɴɢꜱ Aɴᴅ Sᴇᴛ Mᴏᴅᴇ Tᴏ Exᴛʀᴀᴄᴛ**\n**Tʜᴇɴ Uꜱᴇ** /extract **Cᴏᴍᴍᴀɴᴅ**"
        )


@mergeApp.on_message(filters.command(["help"]) & filters.private)
async def help_msg(c: Client, m: Message):
    await m.reply_text(
        text="""**Fᴏʟʟᴏᴡ Tʜᴇsᴇ Sᴛᴇᴘs:

• Sᴇɴᴅ 𝟸 Oʀ Mᴏʀᴇ Vɪᴅᴇᴏs Tᴏ Mᴇʀɢᴇ
• Sᴇɴᴅ Tʜᴇ Cᴜsᴛᴏᴍ ᴛʜᴜᴍʙɴᴀɪʟ (ᴏᴘᴛɪᴏɴᴀʟ)
• Aғᴛᴇʀ Sᴇɴᴅɪɴɢ Aʟʟ Fɪʟᴇs Sᴇʟᴇᴄᴛ Mᴇʀɢᴇ Oᴘᴛɪᴏɴ
• Cʜᴏᴏsᴇ Uᴘʟᴏᴀᴅ Mᴏᴅᴇ
• Rᴇɴᴀᴍᴇ Iғ Yᴏᴜ Wᴀɴᴛ Tᴏ Gɪᴠᴇ Cᴜsᴛᴏᴍ Fɪʟᴇ Nᴀᴍᴇ**""",
        quote=True,
        reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton("Close 🔒", callback_data="close")]]
        ),
    )


@mergeApp.on_message(filters.command(["about"]) & filters.private)
async def about_handler(c: Client, m: Message):
    await m.reply_text(
        text="""
**ᴡʜᴀᴛ's ɴᴇᴡ:**
❍ ʙᴀɴ/ᴜɴʙᴀɴ ᴜsᴇʀs
❍ ᴇxᴛʀᴀᴄᴛ ᴀʟʟ ᴀᴜᴅɪᴏs ᴀɴᴅ sᴜʙᴛɪᴛʟᴇs ғʀᴏᴍ ᴛᴇʟᴇɢʀᴀᴍ ᴍᴇᴅɪᴀ
❍ ᴍᴇʀɢᴇ ᴠɪᴅᴇᴏ + ᴀᴜᴅɪᴏ 
❍ ᴍᴇʀɢᴇ ᴠɪᴅᴇᴏ + sᴜʙᴛɪᴛʟᴇs
❍ ᴜᴘʟᴏᴀᴅ ᴛᴏ ᴅʀɪᴠᴇ ᴜsɪɴɢ ʏᴏᴜʀ ᴏᴡɴ ʀᴄʟᴏɴᴇ ᴄᴏɴғɪɢ
❍ ᴍᴇʀɢᴇᴅ ᴠɪᴅᴇᴏ ᴘʀᴇsᴇʀᴠᴇs ᴀʟʟ sᴛʀᴇᴀᴍs ᴏғ ᴛʜᴇ ғɪʀsᴛ ᴠɪᴅᴇᴏ ʏᴏᴜ sᴇɴᴅ (ɪ.ᴇ ᴀʟʟ ᴀᴜᴅɪᴏᴛʀᴀᴄᴋs/sᴜʙᴛɪᴛʟᴇs)
➖➖➖➖➖➖➖➖➖➖➖➖➖
**ғᴇᴀᴛᴜʀᴇs**
🔰 ᴍᴇʀɢᴇ ᴜᴘᴛᴏ 𝟷𝟶 ᴠɪᴅᴇᴏ ɪɴ ᴏɴᴇ 
🔰 ᴜᴘʟᴏᴀᴅ ᴀs ᴅᴏᴄᴜᴍᴇɴᴛs/ᴠɪᴅᴇᴏ
🔰 ᴄᴜsᴛᴏᴍs ᴛʜᴜᴍʙɴᴀɪʟ sᴜᴘᴘᴏʀᴛ
🔰 ᴜsᴇʀs ᴄᴀɴ ʟᴏɢɪɴ ᴛᴏ ʙᴏᴛ ᴜsɪɴɢ ᴘᴀssᴡᴏʀᴅ
🔰 ᴏᴡɴᴇʀ ᴄᴀɴ ʙʀᴏᴀᴅᴄᴀsᴛ ᴍᴇssᴀɢᴇ ᴛᴏ ᴀʟʟ ᴜsᴇʀs
		""",
        quote=True,
        reply_markup=InlineKeyboardMarkup(
            [
                [InlineKeyboardButton(" • Oᴡɴᴇʀ • ", url="https://t.me/anshXbot0")],
                [
                    InlineKeyboardButton(
                        " • Sᴏᴜʀᴄᴇ Cᴏᴅᴇ • ", url="https://gitlab.com/doona-merger/ansh"
                    ),
                    InlineKeyboardButton(
                        " • Uᴘᴅᴀᴛᴇꜱ • ", url=f"https://t.me/twg_arena"
                    ),
                ],
                [InlineKeyboardButton("Close 🔒", callback_data="close")],
            ]
        ),
    )


@mergeApp.on_message(
    filters.command(["savethumb", "setthumb", "savethumbnail"]) & filters.private
)
async def save_thumbnail(c: Client, m: Message):
    if m.reply_to_message:
        if m.reply_to_message.photo:
            await photo_handler(c, m.reply_to_message)
        else:
            await m.reply(text="**Pʟᴇᴀꜱᴇ Rᴇᴘʟʏ Tᴏ A Vᴀʟɪᴅ Pʜᴏᴛᴏ**")
    else:
        await m.reply(text="**Pʟᴇᴀꜱᴇ Rᴇᴘʟʏ Tᴏ A Mᴇꜱꜱᴀɢᴇ**")
    return


@mergeApp.on_message(filters.command(["showthumbnail"]) & filters.private)
async def show_thumbnail(c: Client, m: Message):
    try:
        user = UserSettings(m.from_user.id, m.from_user.first_name)
        thumb_id = user.thumbnail
        LOCATION = f"downloads/{str(m.from_user.id)}_thumb.jpg"
        if os.path.exists(LOCATION):
            await m.reply_photo(
                photo=LOCATION, caption="**Yᴏᴜʀ Cᴜꜱᴛᴏᴍ Tʜᴜᴍʙɴᴀɪʟ ❤️**", quote=True
            )
        elif thumb_id is not None :
            await c.download_media(message=str(thumb_id), file_name=LOCATION)
            await m.reply_photo(
                photo=LOCATION, caption="**Yᴏᴜʀ Cᴜꜱᴛᴏᴍ Tʜᴜᴍʙɴᴀɪʟ ❤️**", quote=True
            )
        else: 
            await m.reply_text(text="**Cᴜꜱᴛᴏᴍ Tʜᴜᴍʙɴᴀɪʟ Nᴏᴛ Fᴏᴜɴᴅ 😔**", quote=True)
        del user
    except Exception as err:
        LOGGER.info(err)
        await m.reply_text(text="**Cᴜꜱᴛᴏᴍ Tʜᴜᴍʙɴᴀɪʟ Nᴏᴛ Fᴏᴜɴᴅ 😔**", quote=True)


@mergeApp.on_message(filters.command(["deletethumbnail"]) & filters.private)
async def delete_thumbnail(c: Client, m: Message):
    try:
        user = UserSettings(m.from_user.id, m.from_user.first_name)
        user.thumbnail = None
        user.set()
        if os.path.exists(f"downloads/{str(m.from_user.id)}"):
            os.remove(f"downloads/{str(m.from_user.id)}")
            await m.reply_text("**Dᴇʟᴇᴛᴇᴅ Sᴜᴄᴇꜱꜱꜰᴜʟʟʏ ✔️**", quote=True)
            del user
        else: raise Exception("**Tʜᴜᴍʙɴᴀɪʟ Fɪʟᴇ Nᴏᴛ Fᴏᴜɴᴅ**")
    except Exception as err:
        await m.reply_text(text="**Cᴜꜱᴛᴏᴍ Tʜᴜᴍʙɴᴀɪʟ Nᴏᴛ Fᴏᴜɴᴅ ❌**", quote=True)

@mergeApp.on_message(filters.command(["ban","unban"]) & filters.private)
async def ban_user(c:Client,m:Message):
    incoming=m.text.split(' ')[0]
    if incoming == '/ban':
        if m.from_user.id == int(Config.OWNER):
            try:
                abuser_id = int(m.text.split(" ")[1])
                if abuser_id == int(Config.OWNER):
                    await m.reply_text("**I Cᴀɴ'ᴛ Bᴀɴ Yᴏᴜ Mᴀꜱᴛᴇʀ,**\n**Pʟᴇᴀꜱᴇ Dᴏɴ'ᴛ Aʙᴀɴᴅᴏɴ Mᴇ.**",quote=True)
                else:
                    try:
                        user_obj: User = await c.get_users(abuser_id)
                        udata  = UserSettings(uid=abuser_id,name=user_obj.first_name)
                        udata.banned=True
                        udata.allowed=False
                        udata.set()
                        await m.reply_text(f"Pooof, {user_obj.first_name} has been **BANNED**",quote=True)
                        acknowledgement = f"""
**Dᴇᴀʀ** {user_obj.first_name},
**I Fᴏᴜɴᴅ Yᴏᴜʀ Mᴇꜱꜱᴀɢᴇꜱ Aɴɴᴏʏɪɴɢ Aɴᴅ Fᴏʀᴡᴀʀᴅᴇᴅ Tʜᴇᴍ Tᴏ Oᴜʀ Tᴇᴀᴍ Oꜰ Mᴏᴅᴇʀᴀᴛᴏʀꜱ Fᴏʀ Iɴꜱᴘᴇᴄᴛɪᴏɴ. Tʜᴇ Mᴏᴅᴇʀᴀᴛᴏʀꜱ Hᴀᴠᴇ Cᴏɴꜰɪʀᴍᴇᴅ Tʜᴇ Rᴇᴘᴏʀᴛ Aɴᴅ Yᴏᴜʀ Aᴄᴄᴏᴜɴᴛ Iꜱ Nᴏᴡ Bᴀɴɴᴇᴅ.**

**Wʜɪʟᴇ Tʜᴇ Aᴄᴄᴏᴜɴᴛ Iꜱ Bᴀɴɴᴇᴅ, Yᴏᴜ Wɪʟʟ Nᴏᴛ Bᴇ Aʙʟᴇ Tᴏ Dᴏ Cᴇʀᴛᴀɪɴ Tʜɪɴɢꜱ, Lɪᴋᴇ Mᴇʀɢɪɴɢ Vɪᴅᴇᴏꜱ/Aᴜᴅɪᴏꜱ/Sᴜʙᴛɪᴛʟᴇꜱ Oʀ Exᴛʀᴀᴄᴛ Aᴜᴅɪᴏꜱ Fʀᴏᴍ Tᴇʟᴇɢʀᴀᴍ Mᴇᴅɪᴀ.**

**Yᴏᴜʀ Aᴄᴄᴏᴜɴᴛ Cᴀɴ Bᴇ Rᴇʟᴇᴀꜱᴇᴅ Oɴʟʏ Bʏ {Config.OWNER_USERNAME}.**"""
                        try:
                            await c.send_message(
                                chat_id=abuser_id,
                                text=acknowledgement
                            )
                        except Exception as e:
                            await m.reply_text(f"**Aɴ Eʀʀᴏʀ Oᴄᴄᴜʀᴇᴅ Wʜɪʟᴇ Sᴇɴᴅɪɴɢ Aᴄᴋɴᴏᴡʟᴇᴅɢᴇᴍᴇɴᴛ**\n\n`{e}`",quote=True)
                            LOGGER.error(e)
                    except Exception as e:
                        LOGGER.error(e)
            except:
                await m.reply_text("**Cᴏᴍᴍᴀɴᴅ:**\n  `/ban <user_id>`\n\n**Usage:**\n  `user_id`: User ID of the user",quote=True,parse_mode=enums.parse_mode.ParseMode.MARKDOWN)
        else:
            await m.reply_text("**(Only for __OWNER__)\nCommand:**\n  `/ban <user_id>`\n\n**Usage:**\n  `user_id`: User ID of the user",quote=True,parse_mode=enums.parse_mode.ParseMode.MARKDOWN)
        return
    elif incoming == '/unban':
        if m.from_user.id == int(Config.OWNER):
            try:
                abuser_id = int(m.text.split(" ")[1])
                if abuser_id == int(Config.OWNER):
                    await m.reply_text("**I Cᴀɴ'ᴛ Bᴀɴ Yᴏᴜ Mᴀꜱᴛᴇʀ,**\n**Pʟᴇᴀꜱᴇ Dᴏɴ'ᴛ Aʙᴀɴᴅᴏɴ Mᴇ.**",quote=True)
                else:
                    try:
                        user_obj: User = await c.get_users(abuser_id)
                        udata  = UserSettings(uid=abuser_id,name=user_obj.first_name)
                        udata.banned=False
                        udata.allowed=True
                        udata.set()
                        await m.reply_text(f"Pooof, {user_obj.first_name} has been **UN_BANNED**",quote=True)
                        release_notice = f"""
**Gᴏᴏᴅ Nᴇᴡꜱ {user_obj.first_name}, Tʜᴇ Bᴀɴ Hᴀꜱ Bᴇᴇɴ Uᴘʟɪꜰᴛᴇᴅ Oɴ Yᴏᴜʀ Aᴄᴄᴏᴜɴᴛ. Yᴏᴜ'ʀᴇ Fʀᴇᴇ Aꜱ A Bɪʀᴅ!**"""
                        try:
                            await c.send_message(
                                chat_id=abuser_id,
                                text=release_notice
                            )
                        except Exception as e:
                            await m.reply_text(f"**Aɴ Eʀʀᴏʀ Oᴄᴄᴜʀᴇᴅ Wʜɪʟᴇ Sᴇɴᴅɪɴɢ Rᴇʟᴇᴀꜱᴇ Nᴏᴛɪᴄᴇ**\n\n`{e}`",quote=True)
                            LOGGER.error(e)                      
                    except Exception as e:
                        LOGGER.error(e)
            except:
                await m.reply_text("**Cᴏᴍᴍᴀɴᴅ:**\n  `/unban <user_id>`\n\n**Usage:**\n  `user_id`: User ID of the user",quote=True,parse_mode=enums.parse_mode.ParseMode.MARKDOWN)
        else:
            await m.reply_text("**(Only for __OWNER__)\nCommand:**\n  `/unban <user_id>`\n\n**Usage:**\n  `user_id`: User ID of the user",quote=True,parse_mode=enums.parse_mode.ParseMode.MARKDOWN)
        return
async def showQueue(c: Client, cb: CallbackQuery):
    try:
        markup = await makeButtons(c, cb.message, queueDB)
        await cb.message.edit(
            text="**Oᴋᴀʏ,**\nNᴏᴡ Sᴇɴᴅ Mᴇ Nᴇxᴛ Vɪᴅᴇᴏ Oʀ Pʀᴇꜱꜱ **Mᴇʀɢᴇ Nᴏᴡ** Bᴜᴛᴛᴏɴ!",
            reply_markup=InlineKeyboardMarkup(markup),
        )
    except ValueError:
        await cb.message.edit("**Sᴇɴᴅ Sᴏᴍᴇ Mᴏʀᴇ Vɪᴅᴇᴏꜱ**")
    return


async def delete_all(root):
    try:
        shutil.rmtree(root)
    except Exception as e:
        LOGGER.info(e)


async def makeButtons(bot: Client, m: Message, db: dict):
    markup = []
    user = UserSettings(m.chat.id, m.chat.first_name)
    if user.merge_mode == 1:
        for i in await bot.get_messages(
            chat_id=m.chat.id, message_ids=db.get(m.chat.id)["videos"]
        ):
            media = i.video or i.document or None
            if media is None:
                continue
            else:
                markup.append(
                    [
                        InlineKeyboardButton(
                            f"{media.file_name}",
                            callback_data=f"showFileName_{i.id}",
                        )
                    ]
                )

    elif user.merge_mode == 2:
        msgs: list[Message] = await bot.get_messages(
            chat_id=m.chat.id, message_ids=db.get(m.chat.id)["audios"]
        )
        msgs.insert(
            0,
            await bot.get_messages(
                chat_id=m.chat.id, message_ids=db.get(m.chat.id)["videos"][0]
            ),
        )
        for i in msgs:
            media = i.audio or i.document or i.video or None
            if media is None:
                continue
            else:
                markup.append(
                    [
                        InlineKeyboardButton(
                            f"{media.file_name}",
                            callback_data=f"tryotherbutton",
                        )
                    ]
                )

    elif user.merge_mode == 3:
        msgs: list[Message] = await bot.get_messages(
            chat_id=m.chat.id, message_ids=db.get(m.chat.id)["subtitles"]
        )
        msgs.insert(
            0,
            await bot.get_messages(
                chat_id=m.chat.id, message_ids=db.get(m.chat.id)["videos"][0]
            ),
        )
        for i in msgs:
            media = i.video or i.document or None

            if media is None:
                continue
            else:
                markup.append(
                    [
                        InlineKeyboardButton(
                            f"{media.file_name}",
                            callback_data=f"tryotherbutton",
                        )
                    ]
                )

    markup.append([InlineKeyboardButton("Mᴇʀɢᴇ", callback_data="merge")])
    markup.append([InlineKeyboardButton("💥 Cʟᴇᴀʀ Fɪʟᴇꜱ", callback_data="cancel")])
    return markup


LOGCHANNEL = Config.LOGCHANNEL
try:
    if Config.USER_SESSION_STRING is None:
        raise KeyError
    LOGGER.info("Starting USER Session")
    userBot = Client(
        name="merge-bot-user",
        session_string=Config.USER_SESSION_STRING,
        no_updates=True,
    )

except KeyError:
    userBot = None
    LOGGER.warning("No User Session, Default Bot session will be used")


if __name__ == "__main__":
    # with mergeApp:
    #     bot:User = mergeApp.get_me()
    #     bot_username = bot.username
    try:
        with userBot:
            userBot.send_message(
                chat_id=int(LOGCHANNEL),
                text="**Bᴏᴛ Bᴏᴏᴛᴇᴅ Wɪᴛʜ Pʀᴇᴍɪᴜᴍ Aᴄᴄᴏᴜɴᴛ,**\n\n**Tʜᴀɴᴋꜱ Fᴏʀ Uꜱɪɴɢ <a href='https://github.com/doona-merger/ansh'>Tʜɪꜱ Rᴇᴘᴏ**</a>",
                disable_web_page_preview=True,
            )
            user = userBot.get_me()
            Config.IS_PREMIUM = user.is_premium
    except Exception as err:
        LOGGER.error(f"{err}")
        Config.IS_PREMIUM = False
        pass

    mergeApp.run()
